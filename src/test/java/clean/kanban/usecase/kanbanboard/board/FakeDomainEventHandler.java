package clean.kanban.usecase.kanbanboard.board;

import clean.kanban.domain.model.kanbanboard.board.event.BoardCreated;
import clean.kanban.domain.model.kanbanboard.workflow.event.StageCreated;
import clean.kanban.domain.model.kanbanboard.workflow.event.WorkflowCreated;
import com.google.common.eventbus.Subscribe;

public class FakeDomainEventHandler {

    public int boardCreatedCount = 0;
    public int workflowCreatedCount = 0;
    public int stageCreatedCount = 0;

    @Subscribe
    public void handleEvent(BoardCreated domainEvent) {
        boardCreatedCount++;
    }

    @Subscribe
    public void handleEvent(WorkflowCreated domainEvent) {
        workflowCreatedCount++;
    }

    @Subscribe
    public void handleEvent(StageCreated domainEvent) {
        stageCreatedCount++;
    }
}
