package clean.kanban.usecase.kanbanboard.board;

import clean.kanban.adapter.gateway.kanbanboard.InMemoryRepository;
import clean.kanban.domain.model.DomainEventBus;
import clean.kanban.domain.model.kanbanboard.board.Board;
import clean.kanban.domain.usecase.Repository;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CreateBoardUseCaseTest {

    private DomainEventBus eventBus;
    private FakeDomainEventHandler fakeDomainEventHandler;

    @Before
    public void setUp(){
        eventBus = new DomainEventBus();
        fakeDomainEventHandler = new FakeDomainEventHandler();
        eventBus.register(fakeDomainEventHandler);
    }

    @Test
    public void creating_a_board_success(){

        Repository<Board> repository = new InMemoryRepository();
        assertEquals(0, repository.findAll().size());

        CreateBoardUseCase createBoardUseCase =
                new CreateBoardUseCase(repository, eventBus);

        CreateBoardInput input = new CreateBoardInput();
        input.setBoardName("Scrum Board");
        input.setUserId("teddy-123-456-789");

        CreateBoardOutput output = new CreateBoardOutput();

        createBoardUseCase.execute(input, output);

        assertNotNull(output.getBoardId());
        assertEquals(1, repository.findAll().size());
        assertNotNull(repository.findById(output.getBoardId()));

        assertEquals(1, fakeDomainEventHandler.boardCreatedCount);

    }




}
