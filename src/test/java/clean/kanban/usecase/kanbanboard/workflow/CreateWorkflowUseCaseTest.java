package clean.kanban.usecase.kanbanboard.workflow;

import clean.kanban.adapter.gateway.kanbanboard.InMemoryRepository;
import clean.kanban.domain.model.DomainEventBus;
import clean.kanban.domain.model.kanbanboard.board.Board;
import clean.kanban.domain.usecase.Repository;
import clean.kanban.domain.model.kanbanboard.workflow.Workflow;
import clean.kanban.usecase.kanbanboard.board.CreateBoardInput;
import clean.kanban.usecase.kanbanboard.board.CreateBoardOutput;
import clean.kanban.usecase.kanbanboard.board.CreateBoardUseCase;
import clean.kanban.usecase.kanbanboard.board.FakeDomainEventHandler;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CreateWorkflowUseCaseTest {

    private Repository<Board> boardRepository;
    private DomainEventBus eventBus;
    private FakeDomainEventHandler fakeDomainEventHandler;
    private WorkflowCreatedHandler workflowCreatedHandler;

    @Before
    public void setUp(){
        boardRepository = new InMemoryRepository();
        eventBus = new DomainEventBus();
        fakeDomainEventHandler = new FakeDomainEventHandler();
        workflowCreatedHandler = new WorkflowCreatedHandler(boardRepository, eventBus);

        eventBus.register(fakeDomainEventHandler);
        eventBus.register(workflowCreatedHandler);
    }

    @Test
    public void creating_a_workflow_success(){

        String boardId = createBoard();
        Board board = boardRepository.findById(boardId);
        assertEquals(0, board.getCommittedWorkflow().size());

        Repository<Workflow> repository = new InMemoryRepository();
        assertEquals(0, repository.findAll().size());

        CreateWorkflowUseCase createWorkflowUseCase =
                new CreateWorkflowUseCase(repository, eventBus);

        CreateWorkflowInput input = new CreateWorkflowInput();
        input.setName("Default Workflow");
//        input.setBoardId("scrum-board-123-456-789");
        input.setBoardId(boardId);

        CreateWorkflowOutput output = new CreateWorkflowOutput();

        createWorkflowUseCase.execute(input, output);

        assertNotNull(output.getId());
        assertEquals(1, repository.findAll().size());
        assertNotNull(repository.findById(output.getId()));

        assertEquals(1, fakeDomainEventHandler.workflowCreatedCount);

        board = boardRepository.findById(boardId);
        assertEquals(1, board.getCommittedWorkflow().size());

    }

    private String createBoard(){
        assertEquals(0, boardRepository.findAll().size());

        CreateBoardUseCase createBoardUseCase =
                new CreateBoardUseCase(boardRepository, eventBus);

        CreateBoardInput input = new CreateBoardInput();
        input.setBoardName("Scrum Board");
        input.setUserId("teddy-123-456-789");

        CreateBoardOutput output = new CreateBoardOutput();

        createBoardUseCase.execute(input, output);

        return output.getBoardId();
    }


}
