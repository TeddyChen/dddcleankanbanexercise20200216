package clean.kanban.usecase.kanbanboard.workflow;

import clean.kanban.adapter.gateway.kanbanboard.InMemoryRepository;
import clean.kanban.domain.model.DomainEventBus;
import clean.kanban.domain.model.kanbanboard.workflow.Workflow;
import clean.kanban.domain.usecase.Repository;
import clean.kanban.usecase.kanbanboard.Stage.CreateStageInput;
import clean.kanban.usecase.kanbanboard.Stage.CreateStageOutput;
import clean.kanban.usecase.kanbanboard.Stage.CreateStageUseCase;
import clean.kanban.usecase.kanbanboard.board.FakeDomainEventHandler;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CreateStageUseCaseTest {

    private DomainEventBus eventBus;
    private FakeDomainEventHandler fakeDomainEventHandler;
    Repository<Workflow> repository;

    @Before
    public void setUp(){
        repository = new InMemoryRepository();
        eventBus = new DomainEventBus();
        fakeDomainEventHandler = new FakeDomainEventHandler();
        eventBus.register(fakeDomainEventHandler);
    }

    @Test
    public void creating_a_stage_success(){
        Workflow workflow = repository.findById(createWorkflow());
        assertNotNull(workflow);
        assertEquals(0, workflow.getStages().size());

        CreateStageUseCase createStageUseCase =
                new CreateStageUseCase(repository, eventBus);

        CreateStageInput input = new CreateStageInput();
        input.setName("To Do");
        input.setWorkflowId(workflow.getId());

        CreateStageOutput output = new CreateStageOutput();

        createStageUseCase.execute(input, output);

        assertEquals(1, workflow.getStages().size());
        assertEquals("To Do",
                repository.findById(workflow.getId()).getStages().get(0).getName());
        assertEquals(1, fakeDomainEventHandler.stageCreatedCount);
    }

    private String createWorkflow(){

        CreateWorkflowUseCase createWorkflowUseCase =
                new CreateWorkflowUseCase(repository, eventBus);

        CreateWorkflowInput input = new CreateWorkflowInput();
        input.setName("Default Workflow");
        input.setBoardId("scrum-board-123-456-789");
        CreateWorkflowOutput output = new CreateWorkflowOutput();

        createWorkflowUseCase.execute(input, output);
        return output.getId();
    }




}
