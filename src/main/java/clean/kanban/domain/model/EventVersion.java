package clean.kanban.domain.model;

public interface EventVersion {

    static final int NUMBER = 1;
}
