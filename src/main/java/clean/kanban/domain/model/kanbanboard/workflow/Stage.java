package clean.kanban.domain.model.kanbanboard.workflow;

import clean.kanban.domain.model.Entity;

public class Stage extends Entity {

    private String workflowId;

    public Stage(String name, String workflowId) {
        super(name);
        this.workflowId = workflowId;
    }
}
