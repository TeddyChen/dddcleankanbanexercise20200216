package clean.kanban.domain.model.kanbanboard.board.event;

import clean.kanban.domain.model.AbstractDomainEvent;

public class WorkflowCommitted extends AbstractDomainEvent {

    private String boardId;
    private String workflowId;

    public WorkflowCommitted(String boardId, String workflowId){
        super(boardId, workflowId);
    }

    @Override
    public String detail() {
        String formatDate = String.format("occurredOn='%1$tY-%1$tm-%1$td %1$tH:%1$tM']", occurredOn());
        String format = String.format(
                "%s[board id='%s', workflow id='%s'] ",
                this.getClass().getSimpleName(),
                this.boardId, this.workflowId);
        return format + formatDate;
    }

}
