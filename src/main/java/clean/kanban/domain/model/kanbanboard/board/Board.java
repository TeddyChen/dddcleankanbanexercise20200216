package clean.kanban.domain.model.kanbanboard.board;

import clean.kanban.domain.model.AggregateRoot;
import clean.kanban.domain.model.kanbanboard.board.event.BoardCreated;
import clean.kanban.domain.model.kanbanboard.board.event.WorkflowCommitted;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Board extends AggregateRoot {

    private String userId;
    private Set<CommittedWorkflow> committedWorkflows;

    public Board(String boardName, String userId) {
        super(boardName);
        this.userId = userId;
        committedWorkflows = new HashSet<>();

        this.addDomainEvent(new BoardCreated(this.getId(), this.getName()));
    }

    public void commitWorkflow(String workflowId){
        CommittedWorkflow committedWorkflow = new CommittedWorkflow(this.getId(), workflowId);
        committedWorkflow.setOrdering(committedWorkflows.size() + 1);
        committedWorkflows.add(committedWorkflow);

        addDomainEvent(new WorkflowCommitted(this.getId(), workflowId));

    }

    public Set<CommittedWorkflow> getCommittedWorkflow() {

        return Collections.unmodifiableSet(committedWorkflows);
    }
}
