package clean.kanban.domain.model.kanbanboard.workflow.event;

import clean.kanban.domain.model.AbstractDomainEvent;

public class WorkflowCreated extends AbstractDomainEvent {

    private String boardId;

    public WorkflowCreated(
            String sourceId,
            String sourceName,
            String boardId) {

        super(sourceId, sourceName);
        this.boardId = boardId;
    }

    public String getBoardId(){
        return boardId;
    }
}
