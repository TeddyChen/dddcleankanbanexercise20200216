package clean.kanban.domain.model.kanbanboard.board.event;

import clean.kanban.domain.model.AbstractDomainEvent;

public class BoardCreated  extends AbstractDomainEvent {

    public BoardCreated(String sourceId, String sourceName) {
        super(sourceId, sourceName);
    }
}
