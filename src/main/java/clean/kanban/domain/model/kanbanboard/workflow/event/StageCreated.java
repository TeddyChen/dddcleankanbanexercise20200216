package clean.kanban.domain.model.kanbanboard.workflow.event;

import clean.kanban.domain.model.AbstractDomainEvent;

public class StageCreated extends AbstractDomainEvent {

    public StageCreated(String sourceId, String sourceName) {
        super(sourceId, sourceName);
    }
}
