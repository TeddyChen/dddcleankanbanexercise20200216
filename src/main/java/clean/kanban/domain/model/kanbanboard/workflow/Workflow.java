package clean.kanban.domain.model.kanbanboard.workflow;

import clean.kanban.domain.model.AggregateRoot;
import clean.kanban.domain.model.kanbanboard.workflow.event.StageCreated;
import clean.kanban.domain.model.kanbanboard.workflow.event.WorkflowCreated;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Workflow extends AggregateRoot {

    private String boardId;
    private List<Stage> stages;

    public Workflow(String name, String boardId) {
        super(name);
        this.boardId = boardId;
        stages = new ArrayList<>();

        this.addDomainEvent(new WorkflowCreated(
                this.getId(),
                this.getName(),
                this.boardId));
    }

    public Stage createStage(String name) {
        Stage stage = new Stage(name, this.getId());
        stages.add(stage);

        addDomainEvent(new StageCreated(stage.getId(), stage.getName()));

        return stage;
    }

    public List<Stage> getStages() {
        return Collections.unmodifiableList(stages);
    }
}
