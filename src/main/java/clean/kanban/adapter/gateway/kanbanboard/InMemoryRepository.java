package clean.kanban.adapter.gateway.kanbanboard;

import clean.kanban.domain.model.AggregateRoot;
import clean.kanban.domain.model.kanbanboard.board.Board;
import clean.kanban.domain.usecase.Repository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InMemoryRepository<T extends AggregateRoot> implements Repository<T> {

    private List<T> aggregates;

    public InMemoryRepository(){
        aggregates = new ArrayList<>();
    }

    @Override
    public List<T> findAll() {
        return Collections.unmodifiableList(aggregates);
    }

    @Override
    public T findById(String id) {

        for(T each : aggregates){
            if (each.getId().equalsIgnoreCase(id)){
                return each;
            }
        }
        return null;
    }

    @Override
    public T findFirstByName(String name) {
        for(T each : aggregates){
            if (each.getName().equalsIgnoreCase(name)){
                return each;
            }
        }
        return null;
    }

    @Override
    public T save(T arg) {
        if (aggregates.contains(arg)) {
            return arg;
        }
        else if (aggregates.add(arg)){
            return arg;
        }
        else
            return null;
    }

    @Override
    public boolean remove(T arg) {
        return aggregates.remove(arg);
    }
}
