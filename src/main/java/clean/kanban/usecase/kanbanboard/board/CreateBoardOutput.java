package clean.kanban.usecase.kanbanboard.board;

public class CreateBoardOutput {
    private String boardId;


    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }
}
