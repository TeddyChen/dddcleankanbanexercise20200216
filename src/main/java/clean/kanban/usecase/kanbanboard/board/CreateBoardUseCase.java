package clean.kanban.usecase.kanbanboard.board;

import clean.kanban.domain.model.DomainEvent;
import clean.kanban.domain.model.DomainEventBus;
import clean.kanban.domain.model.kanbanboard.board.Board;
import clean.kanban.domain.usecase.Repository;

public class CreateBoardUseCase {

    private Repository<Board> repository;
    private DomainEventBus eventBus;

    public CreateBoardUseCase(
            Repository<Board> repository,
            DomainEventBus eventBus) {
        this.repository = repository;
        this.eventBus = eventBus;
    }

    public void execute(
            CreateBoardInput createBoardInput,
            CreateBoardOutput createBoardOutput) {

        Board board = new Board(createBoardInput.getBoardName(),
                createBoardInput.getUserId());

        repository.save(board);
        eventBus.postAll(board);

        createBoardOutput.setBoardId(board.getId());

    }
}
