package clean.kanban.usecase.kanbanboard.board;

public class CreateBoardInput {
    private String boardName;
    private String userId;

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
