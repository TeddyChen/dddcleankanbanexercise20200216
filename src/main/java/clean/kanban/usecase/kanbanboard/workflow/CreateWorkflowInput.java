package clean.kanban.usecase.kanbanboard.workflow;

public class CreateWorkflowInput {
    private String name;
    private String boardId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }
}
