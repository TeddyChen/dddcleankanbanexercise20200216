package clean.kanban.usecase.kanbanboard.workflow;

public class CreateWorkflowOutput {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
