package clean.kanban.usecase.kanbanboard.workflow;

import clean.kanban.domain.model.DomainEventBus;
import clean.kanban.domain.model.kanbanboard.board.Board;
import clean.kanban.domain.model.kanbanboard.board.event.BoardCreated;
import clean.kanban.domain.model.kanbanboard.workflow.event.StageCreated;
import clean.kanban.domain.model.kanbanboard.workflow.event.WorkflowCreated;
import clean.kanban.domain.usecase.Repository;
import com.google.common.eventbus.Subscribe;

public class WorkflowCreatedHandler {


    private Repository<Board> boardRepository;
    private DomainEventBus eventBus;

    public WorkflowCreatedHandler(Repository boardRepository, DomainEventBus eventBus) {
        this.boardRepository = boardRepository;
        this.eventBus = eventBus;
    }

    public Class<WorkflowCreated> subscribedToEventType() {
        return WorkflowCreated.class;
    }

    @Subscribe
    public void handleEvent(WorkflowCreated domainEvent) {

        Board board = boardRepository.findById(domainEvent.getBoardId());
        board.commitWorkflow(domainEvent.getId());
        boardRepository.save(board);
        eventBus.postAll(board);
    }
}
