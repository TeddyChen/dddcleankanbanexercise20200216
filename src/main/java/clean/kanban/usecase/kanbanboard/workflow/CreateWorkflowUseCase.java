package clean.kanban.usecase.kanbanboard.workflow;

import clean.kanban.domain.model.DomainEventBus;
import clean.kanban.domain.model.kanbanboard.workflow.Workflow;
import clean.kanban.domain.usecase.Repository;

public class CreateWorkflowUseCase {

    private Repository<Workflow> repository;
    private DomainEventBus eventBus;

    public CreateWorkflowUseCase(
            Repository<Workflow> repository,
            DomainEventBus eventBus) {
        this.repository = repository;
        this.eventBus = eventBus;
    }

    public void execute(
            CreateWorkflowInput input,
            CreateWorkflowOutput output) {

        Workflow workflow = new Workflow(input.getName(),
                input.getBoardId());

        repository.save(workflow);
        eventBus.postAll(workflow);

        output.setId(workflow.getId());

    }
}
