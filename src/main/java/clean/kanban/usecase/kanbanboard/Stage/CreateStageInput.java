package clean.kanban.usecase.kanbanboard.Stage;

public class CreateStageInput {
    private String name;
    private String workflowId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkflowId() {
        return workflowId;
    }

    public void setWorkflowId(String workflowId) {
        this.workflowId = workflowId;
    }
}
