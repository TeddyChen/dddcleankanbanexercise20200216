package clean.kanban.usecase.kanbanboard.Stage;

public class CreateStageOutput {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
