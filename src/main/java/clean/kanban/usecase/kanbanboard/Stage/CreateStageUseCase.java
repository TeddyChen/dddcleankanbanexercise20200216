package clean.kanban.usecase.kanbanboard.Stage;

import clean.kanban.domain.model.DomainEventBus;
import clean.kanban.domain.model.kanbanboard.workflow.Stage;
import clean.kanban.domain.model.kanbanboard.workflow.Workflow;
import clean.kanban.domain.usecase.Repository;

public class CreateStageUseCase {

    private Repository<Workflow> repository;
    private DomainEventBus eventBus;

    public CreateStageUseCase(
            Repository<Workflow> repository,
            DomainEventBus eventBus) {
        this.repository = repository;
        this.eventBus = eventBus;
    }

    public void execute(
            CreateStageInput input,
            CreateStageOutput output) {

        Workflow workflow = repository.findById(input.getWorkflowId());
        Stage stage = workflow.createStage(input.getName());

        repository.save(workflow);
        eventBus.postAll(workflow);

        output.setId(stage.getId());

    }
}
